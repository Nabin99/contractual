//
//  EmployerJobDetailsController.swift
//  Contractual
//
//  Created by Apple on 8/2/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import SwiftSpinner

class EmployerJobDetailsController: UIViewController {

    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var imageIndicator: UIActivityIndicatorView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var jobProvider: UILabel!
    @IBOutlet weak var jobDetailsWebView: UIWebView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var contact: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var noOfPeople: UILabel!
    @IBOutlet weak var compensation: UILabel!
    @IBOutlet weak var location: UILabel!
    
    var selectedJob : Job!
    
    var indicator : CustomActivityIndicator!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator = CustomActivityIndicator(view: self.view)


        if(selectedJob != nil){
            jobTitle.text = selectedJob.getTitle()
            jobProvider.text = "Job provided by \(selectedJob.getJobProvider())"
            address.text = selectedJob.getAddress()
            contact.text = selectedJob.getContact()
            email.text = selectedJob.getEmail()
            startDate.text = selectedJob.getStartDate()
            noOfPeople.text = selectedJob.getNoOfPeople()
            compensation.text = selectedJob.getCompensation()
            location.text = selectedJob.getLocation()
            
            var jobDetails = selectedJob.getJobDetails()
            if(jobDetails == "0" || jobDetails == ""){
                jobDetails = "No Description Available"
            }
            let det = "<div style=\"font-family: Helvetica font-size:16\"> \(jobDetails) </div>"
            jobDetailsWebView.loadHTMLString(det, baseURL: nil)
            
            let url = IMAGE_URL_JOB + selectedJob.getImageName()
            
            imageIndicator.startAnimating()
            Alamofire.request(.GET,url)
                .responseImage{ response in
                    if let image = response.result.value {
                        self.jobImage.image = image
                    }
                    else {
                        self.jobImage.contentMode = UIViewContentMode.ScaleAspectFit
                        self.jobImage.image = UIImage(named: "no_image")
                    }
                    self.imageIndicator.stopAnimating()
                    self.imageIndicator.hidesWhenStopped = true
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteJobAction(sender: UIButton) {
            deleteJobComplete()
    }

    
    func deleteJobComplete(){
        let alert = UIAlertController(title: "Delete Job", message: "Are you sure you want to delete this Job?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {action in self.delete()}))
        alert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func delete(){
        let url = DELETEJOB + selectedJob.getJobId()
        
        SwiftSpinner.show("Deleting job..")

        Alamofire.request(.GET,url)
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        if(value["success"] as! Bool == true){
                            SwiftSpinner.hide()
                            let alert = UIAlertController(title: "Delete Successful", message: "You have successfully deleted current job..", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.goToEmployerPage()}))
                            self.presentViewController(alert, animated: true, completion: nil)
                        } else {
                            SwiftSpinner.show(SOMETHINGWENTWRONG).addTapHandler({
                                SwiftSpinner.hide()
                            }, subtitle: TAPRETURN)
                        }
                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
        }

    }
    
    func goToEmployerPage(){
        let storyBoard = UIStoryboard(name: "Employer", bundle: nil)
        let employer:EmployerTabBarController = storyBoard.instantiateViewControllerWithIdentifier("employer") as! EmployerTabBarController
        self.presentViewController(employer, animated: true, completion: nil)
    }
}
