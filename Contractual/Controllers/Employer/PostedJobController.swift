//
//  PostedJobController.swift
//  Contractual
//
//  Created by Apple on 8/1/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import SwiftyJSON
import SwiftSpinner

class PostedJobController: UITableViewController {
    
    var jobList : Array<Job> = Array()
    var indicator : CustomActivityIndicator!
    var selectedJob : Job!


    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let hexColor = HexColor()
//        self.navigationController?.navigationBar.barTintColor = hexColor.UIColorFromRGB("cb1f2e")
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

        self.refreshControl?.addTarget(self, action: #selector(FindJobController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        indicator = CustomActivityIndicator(view: self.view)
        getAllPostedJobs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return jobList.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("postedJobCell", forIndexPath: indexPath) as! PostedJobCell
        
        if(jobList.count > 0){
        let job = jobList[indexPath.row]
        cell.jobTitle.text = job.getTitle()
        cell.jobProvider.text = job.getJobProvider()
        cell.jobDetails.text = job.getJobDetails()
        
        cell.jobImage.image = nil
        
        
        let url = IMAGE_URL_JOB + job.getImageName()
        
        cell.imageIndicatorView.startAnimating()
        Alamofire.request(.GET, url)
            .responseImage{ response in
                if let image = response.result.value {
                    cell.jobImage.image = image
                } else {
                    cell.jobImage.image = UIImage(named: "no_image")
                }
                cell.imageIndicatorView.stopAnimating()
                cell.imageIndicatorView.hidesWhenStopped = true
        }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let job = jobList[indexPath.row]
        selectedJob = job
        self.performSegueWithIdentifier("jobDetails", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "jobDetails" {
            let jobDetails : EmployerJobDetailsController = segue.destinationViewController as! EmployerJobDetailsController
            jobDetails.selectedJob = selectedJob
        }

    }
   
    
    func getAllPostedJobs(){
        
        if(!(refreshControl?.refreshing)!){
            SwiftSpinner.show("Getting your posted jobs..")
        }
        
        let usersPref = UsersPref()
        let id = usersPref.getId()
        let url = POSTEDJOBS + id
        Alamofire.request(.GET,url)
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                   
                    if let value = response.result.value {
                        SwiftSpinner.hide()
                        //clear joblist when new server request is completed
                        self.jobList = Array()
                        let json = JSON(value)
                        self.parseResponse(json)                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
                
                if((self.refreshControl?.refreshing)!){
                    self.refreshControl?.endRefreshing()
                    self.view.userInteractionEnabled = true
                } 
        }
    }
    
    func parseResponse(json:JSON){
        if(json.count > 0){
            
            for i in 0 ..< json.count{
                let job_id = json[i]["job_id"].stringValue
                let title = json[i]["title"].stringValue
                let user_id = json[i]["user_id"].stringValue
                let job_image = json[i]["job_image"].stringValue
                let jobProvider = "\(json[i]["first_name"].stringValue) \(json[i]["last_name"].stringValue)"
                let jobDetails = json[i]["description"].stringValue
                let jobType = json[i]["job_type"].stringValue
                let address = "\(json[i]["city"].stringValue), \(json[i]["city"].stringValue),\(json[i]["country"].stringValue)"
                let contact = json[i]["contact"].stringValue
                let email = json[i]["email"].stringValue
                let startDate = json[i]["start_date"].stringValue
                let noOfPeople = json[i]["number"].stringValue
                let compensation = json[i]["compensation"].stringValue
                let location = json[i]["country"].stringValue
                
                let newJob: Job = Job(job_id: job_id, title: title, user_id: user_id, imageName: job_image, jobProvider: jobProvider, jobDetails: jobDetails, jobType: jobType, address: address, contact: contact, email: email, startDate: startDate, noOfPeople: noOfPeople, compensation: compensation, location: location)
                jobList.append(newJob)
            }
        } else {
            JLToast.makeText("No Jobs Available").show()
        }
        
        
        self.tableView.reloadData()
    }


    func handleRefresh(refreshControl:UIRefreshControl){
        self.view.userInteractionEnabled = false
        getAllPostedJobs()
    }
}
