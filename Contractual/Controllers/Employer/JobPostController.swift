//
//  JobPostController.swift
//  Contractual
//
//  Created by Apple on 8/1/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import AssetsLibrary
import YangMingShan
import SwiftSpinner

class JobPostController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate,YMSPhotoPickerViewControllerDelegate

{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var jobTitle: UITextField!
    @IBOutlet weak var jobDescription: UITextField!
    @IBOutlet weak var noOfPeople: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var phoneNo: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var jobType: UITextField!
    @IBOutlet weak var startDate: UITextField!
    
    @IBOutlet weak var postJobButtonView: UIButton!
    @IBOutlet weak var categoryButtonView: UIButton!
    
    @IBOutlet weak var compensationType: UITextField!
    @IBOutlet weak var compensation: UITextField!
    @IBOutlet weak var imageName: UILabel!
    var indicator : CustomActivityIndicator!
    
    var jobCategoryList : Array<JobCategory> = Array()
    
    var imagePicker: UIImagePickerController!
    
    var categoryFromPicker:String = ""
    var categoryId:String = ""
    var imageString:String = ""
    var categoryTitle:String = ""
    var jobId:Int = 0
     var imageStringArray: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        
//        let hexColor = HexColor()
//        self.navigationController?.navigationBar.barTintColor = hexColor.UIColorFromRGB("#2196F3")
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

        imagePicker = UIImagePickerController()
        indicator = CustomActivityIndicator(view: self.view)
        compensationType.delegate = self
        startDate.delegate = self
        scrollView.contentSize.height = 620
        self.automaticallyAdjustsScrollViewInsets = false
         compensationType.addTarget(self, action: #selector(JobPostController.showCompensationTypeAlert), forControlEvents: UIControlEvents.TouchDown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func categoryChooseAction(sender: UIButton) {
        
        SwiftSpinner.show("Please wait....")

        Alamofire.request(.GET,JOBCATEGORY)
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        SwiftSpinner.hide()
                        self.parseResponse(value as! NSArray)
                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
        }

        
    }

    @IBAction func uploadImageAction(sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum)
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
            imagePicker.allowsEditing = false
            presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Gallery Error", message: "Gallery Not Available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func postJobAction(sender: UIButton) {
        if(jobTitle.text == "" || jobDescription.text == "" || noOfPeople.text == "" || city.text == "" || street == "" || phoneNo == "" || emailAddress == "" || jobType.text == "" || startDate == "" || compensation.text == ""){
            JLToast.makeText("Please fill all the fields").show()
        } else if(categoryId == ""){
            JLToast.makeText("Please choose your job category").show()
        } else if(imageString == ""){
            JLToast.makeText("Please choose your job image").show()
        } else {
            
            
            let usersPref = UsersPref()
            let id = usersPref.getId()
            
            let parameters = ["title": jobTitle.text! as String,"job_image": imageString,"description": jobDescription.text! as String, "city": city.text! as String,"street" : street.text! as String,"contact": phoneNo.text! as String, "email":emailAddress.text! as String,"salary": compensation.text! as String,"compensation": compensationType.text! as String, "job_type":jobType.text! as String,"start_date": startDate.text! as String,"number":noOfPeople.text! as String,"category_id":categoryId as String,"user_id":id as String]
            
            
            SwiftSpinner.show("Posting Job...")
            self.view.userInteractionEnabled = false
            Alamofire.request(.POST,POSTJOB,parameters: parameters)
                .responseJSON{ response in
                    
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            if value["success"] as! Bool == true {
                                SwiftSpinner.hide()
                                self.jobId = value["job_id"] as! Int
                                print(self.jobId)
                                let alert = UIAlertController(title: "Add More Images", message: "You have successfully posted the Job. Do you want to post images related to this job?", preferredStyle: .Alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.goToHomePage(1)}))
                                alert.addAction(UIAlertAction(title: "Add Images", style: .Default, handler: {action in self.addMoreImages()}))
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            else {
                                SwiftSpinner.show(SOMETHINGWENTWRONG).addTapHandler({
                                    SwiftSpinner.hide()
                                }, subtitle: TAPRETURN)
                            }
                        }
                    case .Failure(let error):
                        SwiftSpinner.show(error.localizedDescription).addTapHandler({
                            SwiftSpinner.hide()
                            }, subtitle: TAPRETURN)
                    }
                    self.view.userInteractionEnabled = true
        }
    }
    }
    
    override func viewWillLayoutSubviews() {
        postJobButtonView.layer.cornerRadius = 5
        postJobButtonView.clipsToBounds = true
    }
    
    func parseResponse(value:NSArray){
        let jsonArray = value
        for i in 0  ..< jsonArray.count  {
            let details = jsonArray[i] as! NSDictionary
            
            let category_id = details["category_id"] as! String
            let title = details["title"] as! String
            let parent_id = details["parent_id"] as! String
            let status = details["status"] as! String
            
            let category:JobCategory = JobCategory(title: title, categoryId: category_id, parentId: parent_id, status: status)
            jobCategoryList.append(category)
        }
        if(jobCategoryList.count > 0){
            categoryTitle = jobCategoryList[0].getTitle()
            categoryFromPicker = jobCategoryList[0].getCategoryId()
        }
        createActionSheetForCategory()
       
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(jobCategoryList.count > 0){
            return jobCategoryList.count
        }
        return 0
    }
    
    // Return the title of each row in your picker ... In my case that will be the profile name or the username string
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        if(jobCategoryList.count > 0){
            let category = jobCategoryList[row]
            return category.getTitle()
        }
            return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       let category = jobCategoryList[row]
        categoryTitle = category.getTitle()
        categoryFromPicker = category.getCategoryId()
    }

    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if info[UIImagePickerControllerOriginalImage] != nil
        {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
            let imageData: NSData = UIImageJPEGRepresentation(image, 0.5)!
            
            imageString = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            
                if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
                    
                    ALAssetsLibrary().assetForURL(referenceUrl, resultBlock: { asset in
                        
                        let fileName = asset.defaultRepresentation().filename()
                            self.imageName.text = fileName
                        }, failureBlock: nil)
                }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "No Image", message: "No image found!!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            
        }

    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    
    func createActionSheetForCategory(){
        
        let alert = UIAlertController(title: "", message:"", preferredStyle: .ActionSheet)
        alert.modalInPopover = true
        
        //Create a frame (placeholder/wrapper) for the picker and then create the picker
        let pickerFrame: CGRect = CGRectMake(0, 0, 270, 100); // CGRectMake(left), top, width, height) - left and top are like margins
        let picker: UIPickerView = UIPickerView(frame: pickerFrame);
        
        //set the pickers datasource and delegate
        picker.delegate = self
        picker.dataSource = self
        
        let cancelAction = UIAlertAction(title: "OK", style: .Default, handler: {action in self.chooseCategory(self.categoryTitle)})
        alert.addAction(cancelAction)
        alert.view.addSubview(picker)
        
        // now add some constraints to make sure that the alert resizes itself
        let cons:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.GreaterThanOrEqual, toItem: picker, attribute: NSLayoutAttribute.Height, multiplier: 1.00, constant: 100)
        
        alert.view.addConstraint(cons)
        
        let cons2:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.GreaterThanOrEqual, toItem: picker, attribute: NSLayoutAttribute.Width, multiplier: 1.00, constant: 20)
        
        alert.view.addConstraint(cons2)
        
//        let horizontalConstraint = NSLayoutConstraint(item: picker, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: alert.view, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
//        picker.addConstraint(horizontalConstraint)
//        
//        let verticalConstraint = NSLayoutConstraint(item: picker, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: alert.view, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 10)
//        view.addConstraint(verticalConstraint)
//        
//        let widthConstraint = NSLayoutConstraint(item: picker, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100)
//        picker.addConstraint(widthConstraint)
//        
//        let heightConstraint = NSLayoutConstraint(item: picker, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100)
//        picker.addConstraint(heightConstraint)
        
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func dateAction(sender: UITextField) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(JobPostController.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func chooseCategory(title: String){
        categoryButtonView.setTitle("Category(\(title))", forState: .Normal)
        categoryId = categoryFromPicker
    }
    
    func datePickerValueChanged(sender: UIDatePicker){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        startDate.text = dateFormatter.stringFromDate(sender.date)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    @IBAction func compensationTypeAction(sender: UITextField) {
       print("asdfasdfasdfasdfasdf")
        
    }
    
    func showCompensationTypeAlert(){
        let alert = UIAlertController(title: "Compensation Type", message: "", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "per hour", style: .Default, handler: {action in self.addCompensationType("per hour") }))
        alert.addAction(UIAlertAction(title: "per day", style: .Default, handler: {action in self.addCompensationType("per day") }))
        alert.addAction(UIAlertAction(title: "per month", style: .Default, handler: {action in self.addCompensationType("per month") }))
        alert.addAction(UIAlertAction(title: "per week", style: .Default, handler: {action in self.addCompensationType("per week") }))
        alert.addAction(UIAlertAction(title: "salary", style: .Default, handler: {action in self.addCompensationType("salary") }))
        self.presentViewController(alert, animated: true, completion: nil)

    }
    
    func addCompensationType(type:String){
        compensationType.text = type
    }
    
    func goToHomePage(index: Int){
        //Go to Home Page
        let home:EmployerTabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("employer") as! EmployerTabBarController
        home.selectedIndex = index
        self.presentViewController(home, animated: true, completion: nil)
    }
}
