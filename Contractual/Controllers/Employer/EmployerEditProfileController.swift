//
//  EmployerEditProfileController.swift
//  Contractual
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import SwiftCountryPicker
import JLToast
import Alamofire
import AssetsLibrary
import SwiftSpinner


class EmployerEditProfileController: UIViewController,CountryPickerDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    var userDetail : UserDetails!
    
    @IBOutlet weak var userTypeBtnView: UIButton!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var contactField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var editBtnView: UIButton!
    @IBOutlet weak var imageName: UILabel!
    
    var imagePicker: UIImagePickerController!
    
    var imageString:String = ""
    var indicator : CustomActivityIndicator!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        let countryPicker = CountryPicker(frame: CGRectMake(0,0,self.view.frame.size.width,200))
        countryPicker.countryDelegate = self
        countryTextField.inputView = countryPicker
        countryTextField.delegate = self
        
        if(userDetail != nil){
            firstNameField.text = userDetail.getFirstName()
            lastNameField.text = userDetail.getLastName()
            emailField.text = userDetail.getEmail()
            contactField.text = userDetail.getPhone()
            countryTextField.text = userDetail.getCountry()
            streetField.text = userDetail.getStreet()
            cityField.text = userDetail.getCity()
            descriptionField.text = userDetail.getUserDescription()
            imageName.text = userDetail.getUserImage()
        }
        
        indicator = CustomActivityIndicator(view: self.view)

        imagePicker = UIImagePickerController()
        
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func uploadImageAction(sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum)
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
            imagePicker.allowsEditing = false
            presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Gallery Error", message: "Gallery Not Available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }

    }
    
    func countryPicker(picker: CountryPicker, didSelectCountry country: Country) {
        countryTextField.text = country.name
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return false
    }

    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if info[UIImagePickerControllerOriginalImage] != nil
        {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            let imageData: NSData = UIImageJPEGRepresentation(image, 0.5)!
            
            imageString = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            
            if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
                
                ALAssetsLibrary().assetForURL(referenceUrl, resultBlock: { asset in
                    
                    let fileName = asset.defaultRepresentation().filename()
                    self.imageName.text = fileName
                    }, failureBlock: nil)
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "No Image", message: "No image found!!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func editAction(sender: UIButton) {
        if(firstNameField.text! as String == "" || lastNameField.text! as String == "" || userName.text! as String == "" || emailField.text! as String == "" || passwordField.text! as String == "" || confirmPasswordField.text! as String == "" || contactField.text! as String == "" || countryTextField.text! as String == "" || streetField.text! as String == "" || cityField.text! as String == "" || descriptionField.text! as String == ""){
            JLToast.makeText("Please fill all the fields").show()
        } else if (passwordField.text! as String != confirmPasswordField.text! as String){
            JLToast.makeText("Password do not match").show()
            
        }else if imageString == "" && imageName.text! as String == ""{
            JLToast.makeText("No image Selected").show()
        } else {
            sendDataToServer();
        }

    }
    
    func sendDataToServer(){
  
        let userId = userDetail.getUserId()
        print(userId)
        let userDetailEdit : Bool = true
        let groupId = "0"
        let parameters = ["detailsEdit": userDetailEdit,"userId": userId,"groupId": groupId, "firstName": firstNameField.text! as String,"lastName": lastNameField.text! as String,"userName": userName.text! as String, "email": emailField.text! as String,"password" : passwordField.text! as String,"contact": contactField.text! as String, "country":countryTextField.text! as String,"street":streetField.text! as String,"city": cityField.text! as String,"description":descriptionField.text! as String,"image":imageString]
        
        
        SwiftSpinner.show("Updating your profile..")
        self.view.userInteractionEnabled = false
        Alamofire.request(.POST,REGISTERURL,parameters: parameters as? [String : AnyObject])
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        if value["success"] as! Bool == true {
                            SwiftSpinner.hide()
                            let alert = UIAlertController(title: "Profile Edited", message: "You have successfully edited your profile.", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.goToHomePage(2)}))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        else {
                            SwiftSpinner.show(SOMETHINGWENTWRONG).addTapHandler({
                                SwiftSpinner.hide()
                            }, subtitle: TAPRETURN)
                        }

                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
                self.view.userInteractionEnabled = true
        }
        
    }

    func goToHomePage(index: Int){
        //Go to Home Page
        let home:EmployerTabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("employer") as! EmployerTabBarController
        home.selectedIndex = index
        self.presentViewController(home, animated: true, completion: nil)
    }

    
    override func viewWillLayoutSubviews() {
        userTypeBtnView.layer.cornerRadius = 5
        userTypeBtnView.clipsToBounds = true
        editBtnView.layer.cornerRadius = 5
        editBtnView.clipsToBounds = true
    }
    
}
