//
//  InboxController.swift
//  Contractual
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class InboxController: UITableViewController {
    
    var inboxList : Array<Inbox> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.purpleColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        
        let newMessage: Inbox = Inbox(title: "Message 1", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage1: Inbox = Inbox(title: "Message 2", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage2: Inbox = Inbox(title: "Message 3", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage3: Inbox = Inbox(title: "Message 4", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage4: Inbox = Inbox(title: "Message 5", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage5: Inbox = Inbox(title: "Message 6", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage6: Inbox = Inbox(title: "Message 7", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        let newMessage7: Inbox = Inbox(title: "Message 8", date: "2016-23-01", imageName: "image", details: "kjsdhflahshfahjkasdjkfajksdfkakjsd")
        
        
        inboxList.append(newMessage)
        inboxList.append(newMessage1)
        inboxList.append(newMessage2)
        inboxList.append(newMessage3)
        inboxList.append(newMessage4)
        inboxList.append(newMessage5)
        inboxList.append(newMessage6)
        inboxList.append(newMessage7)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

   
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return inboxList.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("inboxCell", forIndexPath: indexPath) as! InboxCell
        
        let inbox = inboxList[indexPath.row]
        cell.title.text = inbox.getTitle()
        cell.date.text = inbox.getDate()
        cell.details.text = inbox.getDetails()

        return cell
    }
}
