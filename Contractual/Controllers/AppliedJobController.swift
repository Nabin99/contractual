//
//  AppliedJobController.swift
//  Contractual
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import SwiftyJSON
import SwiftSpinner

class AppliedJobController: UITableViewController {
    
    var jobList : Array<Job> = Array()
    var indicator : CustomActivityIndicator!
    var selectedJob : Job!

    override func viewDidLoad() {
        super.viewDidLoad()
//        let hexColor = HexColor()
//        self.navigationController?.navigationBar.barTintColor = hexColor.UIColorFromRGB("#2196F3")
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
         self.refreshControl?.addTarget(self, action: #selector(FindJobController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        indicator = CustomActivityIndicator(view: self.view)

        
        getAllJobs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

   

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return jobList.count
    }

 
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("appliedJobCell", forIndexPath: indexPath) as! appliedJobCell

        if(jobList.count > 0){
        let job = jobList[indexPath.row]
        cell.jobTitle.text = job.getTitle()
        cell.jobProvider.text = job.getJobProvider()
        cell.jobDetails.text = job.getJobDetails()
        
        cell.imageName.image = nil

        
        let url = IMAGE_URL_JOB + job.getImageName()
        
        cell.activityIndicator.startAnimating()
        Alamofire.request(.GET, url)
            .responseImage{ response in
                if let image = response.result.value {
                    cell.imageName.image = image
                } else {
                    cell.imageName.image = UIImage(named: "no_image")
                }
                cell.activityIndicator.stopAnimating()
                cell.activityIndicator.hidesWhenStopped = true
        }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let job = jobList[indexPath.row]
        selectedJob = job
        self.performSegueWithIdentifier("jobDetailsfromApply", sender: self)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "jobDetailsfromApply" {
            let jobDetails : JobDetailsController = segue.destinationViewController as! JobDetailsController
            jobDetails.selectedJob = selectedJob
            jobDetails.appliedJobStatus = true
        }
    }
    
    
    func getAllJobs(){
        
        if(!(refreshControl?.refreshing)!){
          SwiftSpinner.show("Fetching Applied Jobs")
        }

        
        let usersPref = UsersPref()
        let id = usersPref.getId()
        let url = APPLIED_JOBS + id
        
        Alamofire.request(.GET,url)
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        SwiftSpinner.hide()
                        //clear joblist when new server request is completed
                        self.jobList = Array()
                        let json = JSON(value)
                        self.parseResponse(json)
                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription, animated: false).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
                
                if((self.refreshControl?.refreshing)!){
                    self.refreshControl?.endRefreshing()
                    self.view.userInteractionEnabled = true
                }
        }
    }
    
    func parseResponse(json:JSON){
        if(json.count > 0){
            for i in 0 ..< json.count{
                let job_id = json[i]["job_id"].stringValue
                let title = json[i]["title"].stringValue
                let user_id = json[i]["user_id"].stringValue
                let job_image = json[i]["job_image"].stringValue
                let jobProvider = "\(json[i]["first_name"].stringValue) \(json[i]["last_name"].stringValue)"
                let jobDetails = json[i]["description"].stringValue
                let jobType = json[i]["job_type"].stringValue
                let address = "\(json[i]["city"].stringValue), \(json[i]["city"].stringValue),\(json[i]["country"].stringValue)"
                let contact = json[i]["contact"].stringValue
                let email = json[i]["email"].stringValue
                let startDate = json[i]["start_date"].stringValue
                let noOfPeople = json[i]["number"].stringValue
                let compensation = json[i]["compensation"].stringValue
                let location = json[i]["country"].stringValue
                
                let newJob: Job = Job(job_id: job_id, title: title, user_id: user_id, imageName: job_image, jobProvider: jobProvider, jobDetails: jobDetails, jobType: jobType, address: address, contact: contact, email: email, startDate: startDate, noOfPeople: noOfPeople, compensation: compensation, location: location)
                jobList.append(newJob)
            }
        } else {
            SwiftSpinner.show(NOAPPLIEDJOBS, animated: false).addTapHandler({
                SwiftSpinner.hide()
            }, subtitle: TAPRETURN)
        }
        
        
        self.tableView.reloadData()
    }

    func handleRefresh(refreshControl:UIRefreshControl){
        self.view.userInteractionEnabled = false
        getAllJobs()
    }

}
