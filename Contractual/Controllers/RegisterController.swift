//
//  RegisterController.swift
//  Contractual
//
//  Created by Apple on 8/16/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import SwiftCountryPicker
import JLToast
import AssetsLibrary
import Alamofire
import SwiftSpinner


class RegisterController: UIViewController, CountryPickerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var contactField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var registerButtonView: UIButton!
    @IBOutlet weak var imageName: UILabel!
    @IBOutlet weak var userTypeBtnView: UIButton!
    @IBOutlet weak var registerAsText: UILabel!
    
    var imagePicker: UIImagePickerController!
    
    var imageString:String = ""
    var indicator : CustomActivityIndicator!
    var userType : String = "Employee"
    var editProfile : Bool = false
    var userDetail : UserDetails!
    var userDetailEdit : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(editProfile){
            self.navigationItem.title = "Edit Profile"
            registerAsText.text = "Registered As:"
            userTypeBtnView.userInteractionEnabled = false
            registerButtonView.setTitle("Edit", forState: .Normal)
            
            if userDetail != nil {
                firstNameField.text = userDetail.getFirstName()
                lastNameField.text = userDetail.getLastName()
                emailField.text = userDetail.getEmail()
                countryTextField.text = userDetail.getCountry()
                cityField.text = userDetail.getCity()
                contactField.text = userDetail.getPhone()
                streetField.text = userDetail.getStreet()
                if(userDetail.getUserDescription() == "0"){
                    descriptionField.text = ""
                } else {
                    descriptionField.text = userDetail.getUserDescription()
                }
                imageName.text = userDetail.getUserImage()
            }
        }
        
        imagePicker = UIImagePickerController()
        self.automaticallyAdjustsScrollViewInsets = false
        
        indicator = CustomActivityIndicator(view: self.view)


        let countryPicker = CountryPicker(frame: CGRectMake(0, 0, self.view.frame.size.width, 200))
        countryPicker.countryDelegate = self
        countryTextField.inputView = countryPicker
        countryTextField.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func countryFieldAction(sender: UITextField) {
      
    }
    
    @IBAction func uploadImageAction(sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum)
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
            imagePicker.allowsEditing = false
            presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Gallery Error", message: "Gallery Not Available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        }

    }
    func countryPicker(picker: CountryPicker, didSelectCountry country: Country){
        countryTextField.text = country.name
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    @IBAction func registerAction(sender: UIButton) {
            if(firstNameField.text! as String == "" || lastNameField.text! as String == "" || userName.text! as String == "" || emailField.text! as String == "" || passwordField.text! as String == "" || confirmPasswordField.text! as String == "" || contactField.text! as String == "" || countryTextField.text! as String == "" || streetField.text! as String == "" || cityField.text! as String == "" || descriptionField.text! as String == ""){
                JLToast.makeText("Please fill all the fields").show()
            } else if (passwordField.text! as String != confirmPasswordField.text! as String){
                JLToast.makeText("Password do not match").show()
                
            }else if imageString == "" && imageName.text! as String == ""{
                JLToast.makeText("No image Selected").show()
            } else {
                sendDataToServer();
            }
          }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if info[UIImagePickerControllerOriginalImage] != nil
        {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            let imageData: NSData = UIImageJPEGRepresentation(image, 0.5)!
            
            imageString = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            
            if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
                
                ALAssetsLibrary().assetForURL(referenceUrl, resultBlock: { asset in
                    
                    let fileName = asset.defaultRepresentation().filename()
                    self.imageName.text = fileName
                    }, failureBlock: nil)
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "No Image", message: "No image found!!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func sendDataToServer(){
        
        var userId:String = "0"
        var groupId:String = "0"
        if(editProfile){
            userDetailEdit = true
            userId = userDetail.getUserId()
        } else {
            if userType == "Employer" {
                groupId = "3"
            } else if userType == "Employee" {
                groupId = "4"
            }
        }
        
        let parameters = ["detailsEdit": userDetailEdit,"userId": userId,"groupId":groupId, "firstName": firstNameField.text! as String,"lastName": lastNameField.text! as String,"userName": userName.text! as String, "email": emailField.text! as String,"password" : passwordField.text! as String,"contact": contactField.text! as String, "country":countryTextField.text! as String,"street":streetField.text! as String,"city": cityField.text! as String,"description":descriptionField.text! as String,"image":imageString]
        
        
            SwiftSpinner.show("Registering your account..")
            self.view.userInteractionEnabled = false
            Alamofire.request(.POST,REGISTERURL,parameters: parameters as? [String : AnyObject])
                .responseJSON{ response in
                
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            SwiftSpinner.hide()
                            if value["success"] as! Bool == true {
                                if(self.editProfile){
                                    let alert = UIAlertController(title: "Profile Edited", message: "You have successfully edited your profile.", preferredStyle: .Alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.goToHomePage(2)}))
                                    self.presentViewController(alert, animated: true, completion: nil)
                                } else {
                                    let alert = UIAlertController(title: "Registration Successful", message: "You have successfully registered", preferredStyle: .Alert)
                                    alert.addAction(UIAlertAction(title: "Login", style: .Default, handler: {action in self.goToLoginPage()}))
                                    self.presentViewController(alert, animated: true, completion: nil)
                                }
                                
                            }
                            else {
                                SwiftSpinner.show(SOMETHINGWENTWRONG, animated: false).addTapHandler({
                                    SwiftSpinner.hide()
                                }, subtitle: TAPRETURN)
                            }

                            }
                    case .Failure(let error):
                        SwiftSpinner.show(error.localizedDescription, animated: false).addTapHandler({
                            SwiftSpinner.hide()
                            }, subtitle: TAPRETURN)
                    }
                    self.view.userInteractionEnabled = true
            }

    }

    @IBAction func userTypeBtnAction(sender: UIButton) {
        let alert = UIAlertController(title: "Choose User", message: "", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Employee", style: .Default, handler: {action in self.setUserTypeButton("Employee") }))
        alert.addAction(UIAlertAction(title: "Employer", style: .Default, handler: {action in self.setUserTypeButton("Employer") }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func setUserTypeButton(type:String){
        userType = type
        userTypeBtnView.setTitle(userType, forState: .Normal)
    }
    
    func goToLoginPage(){
        let loginPage: LoginController = self.storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginController
        self.presentViewController(loginPage, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        userTypeBtnView.setTitle(userType, forState: .Normal)
        self.navigationController?.navigationBarHidden = false
    }
    
    func goToHomePage(index: Int){
        //Go to Home Page
        let home:HomeTabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("home") as! HomeTabBarController
        home.selectedIndex = index
        self.presentViewController(home, animated: true, completion: nil)
    }

    
    override func viewWillLayoutSubviews() {
        userTypeBtnView.layer.cornerRadius = 5
        userTypeBtnView.clipsToBounds = true
        registerButtonView.layer.cornerRadius = 5
        registerButtonView.clipsToBounds = true
    }

}
