//
//  FindJobController.swift
//  Contractual
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import SwiftyJSON
import SwiftSpinner

class FindJobController: UITableViewController,UISearchBarDelegate {
    var jobList : Array<Job> = Array()
    var selectedJob : Job!
    var toastPoint : CGPoint!
    var indicator : CustomActivityIndicator!
    var searchActive : Bool = false
    var filteredJobs : Array<Job> = Array()
    var searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSearchBar()
        self.refreshControl?.addTarget(self, action: #selector(FindJobController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        toastPoint = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        indicator = CustomActivityIndicator(view: self.view)
        getAllJobs()
    }
    
    func UIColorFromRGB(colorCode: String, alpha: Float = 1.0) -> UIColor {
        let scanner = NSScanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt(&color)
        
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
                return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            return filteredJobs.count
        }
        return jobList.count
    }

   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("jobCell", forIndexPath: indexPath) as! JobCell
        
        if(searchActive){
            if(filteredJobs.count > 0){
            let job = filteredJobs[indexPath.row]
            cell.jobTitle.text = job.getTitle()
                print(job.getTitle())
                print(job.getJobProvider())
            cell.jobProvider.text = "Job Provided by \(job.getJobProvider())"
            cell.jobDetails.text = job.getJobDetails()
            
            cell.jobImage.image = nil
            
            let url = IMAGE_URL_JOB + job.getImageName()
            
            cell.activityIndicator.startAnimating()
            Alamofire.request(.GET, url)
                .responseImage{ response in
                    if let image = response.result.value {
                        cell.jobImage.image = image
                    } else {
                        cell.jobImage.image = UIImage(named: "no_image")
                    }
                    cell.activityIndicator.stopAnimating()
                    cell.activityIndicator.hidesWhenStopped = true
                }
            }

        }
    
        else if(jobList.count > 0){
            let job = jobList[indexPath.row]
            cell.jobTitle.text = job.getTitle()
            cell.jobProvider.text = "Job Provided by \(job.getJobProvider())"
            cell.jobDetails.text = job.getJobDetails()
        
            cell.jobImage.image = nil
        
            let url = IMAGE_URL_JOB + job.getImageName()
        
            cell.activityIndicator.startAnimating()
            Alamofire.request(.GET, url)
                .responseImage{ response in
                    if let image = response.result.value {
                        cell.jobImage.image = image
                    } else {
                        cell.jobImage.image = UIImage(named: "no_image")
                    }
                    cell.activityIndicator.stopAnimating()
                    cell.activityIndicator.hidesWhenStopped = true
                    
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if(searchActive){
            let job = filteredJobs[indexPath.row]
            selectedJob = job
        } else {
            let job = jobList[indexPath.row]
            selectedJob = job
        }
        self.performSegueWithIdentifier("jobDetails", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "jobDetails" {
            let jobDetails : JobDetailsController = segue.destinationViewController as! JobDetailsController
            jobDetails.selectedJob = selectedJob
        }
    }
    
    func getAllJobs(){
        
        if(!(refreshControl?.refreshing)!){
           SwiftSpinner.show("Fetching all Jobs")
        }

        let usersPref = UsersPref()
        let id = usersPref.getId()
        
        let url = LATEST_JOBS + id
        Alamofire.request(.GET,url)
            .responseJSON{ response in
                
                switch response.result {
                    
                case .Success:
                    
                    if let value = response.result.value {
                        print(value)
                        SwiftSpinner.hide()
                        // Clear jobList and filteredJobs every time when new server request is successfully made
                        self.jobList = Array()
                        self.filteredJobs = Array()
                        let json = JSON(value)
                        self.parseResponse(json)
                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription, animated: false).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
                
                if((self.refreshControl?.refreshing)!){
                    self.refreshControl?.endRefreshing()
                    self.view.userInteractionEnabled = true

                }
        }
    }
    
    func parseResponse(json: JSON){

        if(json.count > 0){
        
            for i in 0 ..< json.count{
                let job_id = json[i]["job_id"].stringValue
                let title = json[i]["title"].stringValue
                let user_id = json[i]["user_id"].stringValue
                let job_image = json[i]["job_image"].stringValue
                let jobProvider = "\(json[i]["first_name"].stringValue) \(json[i]["last_name"].stringValue)"
                let jobDetails = json[i]["description"].stringValue
                let jobType = json[i]["job_type"].stringValue
                let address = "\(json[i]["city"].stringValue), \(json[i]["city"].stringValue),\(json[i]["country"].stringValue)"
                let contact = json[i]["contact"].stringValue
                let email = json[i]["email"].stringValue
                let startDate = json[i]["start_date"].stringValue
                let noOfPeople = json[i]["number"].stringValue
                let compensation = json[i]["compensation"].stringValue
                let location = json[i]["country"].stringValue
            
                let newJob: Job = Job(job_id: job_id, title: title, user_id: user_id, imageName: job_image, jobProvider: jobProvider, jobDetails: jobDetails, jobType: jobType, address: address, contact: contact, email: email, startDate: startDate, noOfPeople: noOfPeople, compensation: compensation, location: location)
                jobList.append(newJob)
            }
        } else {
            JLToast.makeText("No Jobs Available").show()
        }
        
        
        self.tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCellWithIdentifier("headerCell") as! CustomHeader
        return headerCell
    }
    
    func handleRefresh(refreshControl:UIRefreshControl){
        self.view.userInteractionEnabled = false
        searchBar.text = ""
        searchActive = false
        getAllJobs()
    }
    
    func showSearchBar(){
        searchBar.sizeToFit()
        searchBar.placeholder = "Find a Job"
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }
}
