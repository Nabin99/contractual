//
//  SettingsController.swift
//  Contractual
//
//  Created by Apple on 7/28/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import SwiftyJSON
import SwiftSpinner

class SettingsController: UIViewController {

    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var imageIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var country: UILabel!
    
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var userDescription: UIWebView!
    @IBOutlet weak var wholeView: UIView!
    
    var indicator : CustomActivityIndicator!
    var user : UserDetails!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = false
        
//        self.navigationController?.navigationBar.barTintColor = UIColor.blueColor()
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        indicator = CustomActivityIndicator(view: self.view)
        imageIndicator.hidesWhenStopped = true

        getAllData();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillLayoutSubviews() {
        userImage.layer.cornerRadius = 5
    }
    
    
    @IBAction func editProfileAction(sender: UIButton) {
        if user != nil {
            performSegueWithIdentifier("editProfile", sender: nil)
        }
    }
    
    func getAllData() {
        
        self.wholeView.hidden = true
        SwiftSpinner.show("Accessing your Settings")
        let usersPref = UsersPref()
        let id = usersPref.getId()
        
        let url = PROFILE + id
        
        Alamofire.request(.GET,url)
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                    print(response.result.value)
                    if let value = response.result.value {
                        self.wholeView.hidden = false
                        let json = JSON(value)
                        self.parseResponse(json)
                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription, animated: false).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
                
                SwiftSpinner.hide()
        }

    }
    
    func parseResponse(json: JSON){
        
        let userId = json[0]["user_id"].stringValue
        let firstName = json[0]["first_name"].stringValue
        let lastName = json[0]["last_name"].stringValue
        let emailText = json[0]["employee_email"].stringValue
        let phoneText = json[0]["contact"].stringValue
        let streetText = json[0]["street"].stringValue
        let cityText = json[0]["city"].stringValue
        let countryText = json[0]["country"].stringValue
        let userImageName = json[0]["user_image"].stringValue
        let userDescriptionText = json[0]["user_detail"].stringValue
        
        user = UserDetails(userId: userId, firstName: firstName, lastName: lastName, email: emailText, phone: phoneText, street: streetText, city: cityText, country: countryText, userImage: userImageName, userDescription: userDescriptionText)
        
        userName.text = "\(user.getFirstName()) \(user.getLastName())"
        email.text = user.getEmail()
        phone.text = user.getPhone()
        street.text = user.getStreet()
        city.text = user.getCity()
        country.text = user.getCountry()
        var userDetail = user.getUserDescription()
        if(userDetail == "0"){
            userDetail = "No Description Available"
        }
        let det = "<div style=\"font-family: Helvetica\"> \(userDetail) </div>"
        userDescription.loadHTMLString(det, baseURL: nil)
        
        let url = IMAGE_URL_USERS + user.getUserImage()
        print(url)
        imageIndicator.startAnimating()
        
        Alamofire.request(.GET,url)
            .responseImage{ response in
                if let image = response.result.value {
                    self.userImage.image = image
                } else {
                    self.userImage.image = UIImage(named: "no_image")
                }
                    self.imageIndicator.stopAnimating()
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "editProfile"){
            let destinationController : RegisterController = segue.destinationViewController as! RegisterController
            destinationController.editProfile = true
            destinationController.userDetail = user
        }
    }

    @IBAction func logOutAction(sender: UIButton) {
        
        let alert = UIAlertController(title: "Log Out", message: "Are you sure you want to log out?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {action in self.logOut()}))
        alert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)

    }
    
    func logOut(){
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.removeObjectForKey("user_id")
        prefs.removeObjectForKey("username")
        prefs.removeObjectForKey("email")
        
        //Go to Login Page
        let login:LoginController = self.storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginController
        self.presentViewController(login, animated: true, completion: nil)

        
    }
}
