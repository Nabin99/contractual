//
//  LoginController.swift
//  Contractual
//
//  Created by Apple on 7/22/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftSpinner


class LoginController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var loginImage: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var newAccountText: UILabel!
    @IBOutlet weak var userTypeButtonView: UIButton!
    var activityIndicator:UIActivityIndicatorView!
    var toastPoint : CGPoint!
    var indicator : CustomActivityIndicator!
    var userType : String = "Employee"
    
    let loginButton : FBSDKLoginButton = {
       let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        toastPoint = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        indicator = CustomActivityIndicator(view: self.view)
        loginButton.delegate = self
        //view.addSubview(loginButton)
       
    }
    
    override func viewDidAppear(animated: Bool) {
        let loginStatus = LoginStatus()
        
        if(loginStatus.isLoggedIn() == "4"){
            goToHomePage()
        } else if(loginStatus.isLoggedIn() == "3"){
            goToEmployerPage()
        }
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func signInAction(sender: UIButton) {
   
        if emailTextField.text! == "" || passwordTextField.text! == "" {
                JLToast.makeText(EMPTY_FIELD).show()
            } else {
                if Reachability.isConnectedToNetwork() == true {
                    submitForm()
                } else {
                    JLToast.makeText(NO_CONNECTION).show()
                }
            }
    }
    
    override func viewWillAppear(animated: Bool) {
        userTypeButtonView.setTitle(userType, forState: .Normal)
        print("Will Appear")
       // self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        //self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        loginImage.layer.cornerRadius = loginImage.frame.size.width/2
        loginImage.clipsToBounds = true
        signInButton.layer.cornerRadius = 5
        emailTextField.backgroundColor = UIColor.blackColor()
        emailTextField.alpha = 0.4
        emailTextField.layer.borderColor = UIColor.whiteColor().CGColor
        emailTextField.layer.borderWidth = 2
        emailTextField.clipsToBounds = true
        emailTextField.layer.cornerRadius = 2
        emailTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        passwordTextField.backgroundColor = UIColor.blackColor()
        passwordTextField.alpha = 0.4
        passwordTextField.layer.borderColor = UIColor.whiteColor().CGColor
        passwordTextField.layer.borderWidth = 2
        passwordTextField.clipsToBounds = true
        passwordTextField.layer.cornerRadius = 2
        passwordTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        userTypeButtonView.layer.cornerRadius = 5
        userTypeButtonView.clipsToBounds = true
    }
    
    
    @IBAction func userTypeButtonAction(sender: UIButton) {
        let alert = UIAlertController(title: "Choose User", message: "", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Employee", style: .Default, handler: {action in self.setUserTypeButton("Employee") }))
         alert.addAction(UIAlertAction(title: "Employer", style: .Default, handler: {action in self.setUserTypeButton("Employer") }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func setUserTypeButton(type:String){
        userType = type
        userTypeButtonView.setTitle(userType, forState: .Normal)
    }
    
    func submitForm(){
        
        let email = emailTextField.text! as String
        let password = passwordTextField.text! as String
        
        let parameters = ["username":email, "password": password]
    
        SwiftSpinner.show("Checking your Credentials")
        
        Alamofire.request(.POST,LOGIN_POST,parameters: parameters)
            .responseJSON { response in
                switch response.result
                {
                case .Success:
                    if let JSON = response.result.value {
                        print(response.result.value)
                        let status = JSON["success"] as! Bool
                        if status == true {
                            let details = JSON["user"] as! NSDictionary
                            let username = details["username"] as! String
                            let email = details["email"] as! String
                            let id = details["id"] as! String
                            let group = details["group"] as! String
                            
                            let user:User = User(username: username, email: email, id: id,group: group)
                            
                            //save user Object to UserDefaults
                            let prefs = NSUserDefaults.standardUserDefaults()
                            prefs.setValue(user.getId(), forKey: "user_id")
                            prefs.setValue(user.getUsername(), forKey: "username")
                            prefs.setValue(user.getEmail(), forKey: "email")
                            prefs.setValue(user.getGroup(), forKey: "group")
                            
                            if user.getGroup() == "4" {
                                self.goToHomePage()
                            } else if user.getGroup() == "3"{
                                self.goToEmployerPage()
                            }

                        } else {
                            SwiftSpinner.show(INVALID_CREDENTIAL,animated: false).addTapHandler({
                                SwiftSpinner.hide()
                            }, subtitle: TAPRETURN)
                        }
                    }

                case .Failure(let error):
                    print(error)
                    SwiftSpinner.show(error.localizedDescription, animated: false).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
        }
        
      
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func goToHomePage(){
        //Go to Home Page
        let home:HomeTabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("home") as! HomeTabBarController
        self.presentViewController(home, animated: true, completion: nil)
    }
    
    func goToEmployerPage(){
        let storyBoard = UIStoryboard(name: "Employer", bundle: nil)
        let employer:EmployerTabBarController = storyBoard.instantiateViewControllerWithIdentifier("employer") as! EmployerTabBarController
        self.presentViewController(employer, animated: true, completion: nil)
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print(result)
        fetchProfile();
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func fetchProfile(){
        let parameters = ["fields": "email,first_name,last_name,picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).startWithCompletionHandler({ (connection,user,requestError) -> Void in
        
            if requestError != nil {
                print(requestError)
                return
            }
            
            var fbEmail = user["email"] as? String
            var fbFirstName = user["first_name"] as? String
            let fbLastName = user["last_name"] as? String
            
            var pictureUrl = ""
            
            if let picture = user["picture"] as? NSDictionary, data = picture["data"] as? NSDictionary, url = data["url"] as? String {
                pictureUrl = url
            }
            
            print(fbEmail)
            print(fbFirstName)
            print(fbLastName)
        
        })
    }
    
    @IBAction func newAccountAction(sender: UIButton) {
        performSegueWithIdentifier("newAccount", sender: nil)
    }
}
