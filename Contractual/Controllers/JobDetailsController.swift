//
//  JobDetailsController.swift
//  Contractual
//
//  Created by Apple on 7/22/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JLToast
import SwiftSpinner

class JobDetailsController: UIViewController {

    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var jobProvider: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var contact: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var noOfPeople: UILabel!
    @IBOutlet weak var compensation: UILabel!
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var imageIndicator: UIActivityIndicatorView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var callEmployeeBtn: UIButton!
    @IBOutlet weak var jobDetailsWebView: UIWebView!
    
     var indicator : CustomActivityIndicator!
    
    var activityIndicator:UIActivityIndicatorView!
    var appliedJobStatus:Bool = false
    
    var selectedJob : Job!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator = CustomActivityIndicator(view: self.view)
        self.automaticallyAdjustsScrollViewInsets = false

        //hide apply button if it is from posted job controller
        if(appliedJobStatus == true){
//            applyButton.hidden = true
//            callEmployeeBtn.hidden = false
            applyButton.setTitle("Call Employee", forState: .Normal)
            applyButton.tag = 2
        }else {
//            applyButton.hidden = false
//            callEmployeeBtn.hidden = true
            applyButton.setTitle("Apply For Job", forState: .Normal)
            applyButton.tag = 1
        }

        if(selectedJob != nil){
            jobTitle.text = selectedJob.getTitle()
            jobProvider.text = "Job provided by \(selectedJob.getJobProvider())"
            address.text = selectedJob.getAddress()
            contact.text = selectedJob.getContact()
            email.text = selectedJob.getEmail()
            startDate.text = selectedJob.getStartDate()
            noOfPeople.text = selectedJob.getNoOfPeople()
            compensation.text = selectedJob.getCompensation()
            location.text = selectedJob.getLocation()
            
            var jobDetails = selectedJob.getJobDetails()
            if(jobDetails == "0" || jobDetails == ""){
                jobDetails = "No Description Available"
            }
            let det = "<div style=\"font-family: Helvetica font-size:16\"> \(jobDetails) </div>"
            jobDetailsWebView.loadHTMLString(det, baseURL: nil)

            
            let url = IMAGE_URL_JOB + selectedJob.getImageName()
            
            imageIndicator.startAnimating()
            Alamofire.request(.GET,url)
                .responseImage{ response in
                    if let image = response.result.value {
                        self.jobImage.image = image
                    }
                    else {
                        self.jobImage.contentMode = UIViewContentMode.ScaleAspectFit
                        self.jobImage.image = UIImage(named: "no_image")
                    }
                    self.imageIndicator.stopAnimating()
                    self.imageIndicator.hidesWhenStopped = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func applyBtnAction(sender: UIButton) {
        if(sender.tag == 1){
            let usersPref = UsersPref()
            let userId = usersPref.getId()
            let jobId = selectedJob.getJobId()
            let alert = UIAlertController(title: "Apply for this Job", message: "Please Confirm your application for this job..", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Apply", style: .Default, handler: {action in self.applyForJob(userId,jobId: jobId)}))
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
        } else {
            callEmployee()
        }

    }
    
    override func viewWillLayoutSubviews() {
        jobImage.layer.cornerRadius = 5
        jobImage.clipsToBounds = true
    }
    
    func applyForJob(userId:String,jobId:String) {
        
        
        let parameters = ["id": userId, "job_id": jobId]
        SwiftSpinner.show("Applying....")
        Alamofire.request(.POST,APPLYJOB,parameters: parameters)
            .responseJSON{ response in
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        SwiftSpinner.hide()
                        if value["success"] as! Bool == true{
                            let alert = UIAlertController(title: "Application Successful", message: "You have successfully applied for this job..", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.goToHomePage()}))
                            self.presentViewController(alert, animated: true, completion: nil)

                        } else {
                            JLToast.makeText("Ooops.. Something Went Wrong").show()
                        }
                    }
                case .Failure(let error):
                    SwiftSpinner.show(error.localizedDescription, animated: false).addTapHandler({
                        SwiftSpinner.hide()
                    }, subtitle: TAPRETURN)
                }
        }

    }
    @IBAction func callEmployerAction(sender: UIButton) {
        callEmployee()
            }
    
    func callEmployee(){
        let phone = contact.text!
        
        if(phone != ""){
            print(phone)
            let formatedNumber = phone.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
            let phoneUrl = "tel://\(formatedNumber)"
            print(phoneUrl)
            let url:NSURL = NSURL(string: phoneUrl)!
            UIApplication.sharedApplication().openURL(url)
        } else {
            JLToast.makeText("No Contact Number").show()
        }

    }
    
    func goToHomePage(){
        //Go to Home Page
        let home:HomeTabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("home") as! HomeTabBarController
        home.selectedIndex = 1
        self.presentViewController(home, animated: true, completion: nil)
    }
}
