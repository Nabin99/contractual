//
//  PostedJobCell.swift
//  Contractual
//
//  Created by Apple on 8/1/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class PostedJobCell: UITableViewCell {

    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var imageIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var jobProvider: UILabel!
    @IBOutlet weak var jobDetails: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        jobImage.layer.cornerRadius = 5
        jobImage.clipsToBounds = true
    }

}
