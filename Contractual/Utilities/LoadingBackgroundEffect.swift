//
//  LoadingBackgroundEffect.swift
//  Contractual
//
//  Created by Apple on 7/29/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import UIKit

class LoadingBackgroundEffect: NSObject {
    
    let view: UIView!
    
    init(view: UIView) {
        self.view = view
    }
    
    func fadeOut(){
        view.backgroundColor = UIColor(white: 0.8, alpha: 0.5)
    }
    
    func fadeIn(){
        view.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
    }
    
    func fadeOutWhole(){
        view.backgroundColor = UIColor(white: 1.0, alpha: 0.0)
    }
    
}