//
//  UserDetails.swift
//  Contractual
//
//  Created by Apple on 7/28/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation


class UserDetails : NSObject {
    
    private let userId:String
    private let firstName:String
    private let lastName:String
    private let email:String
    private let phone:String
    private let street:String
    private let city:String
    private let country:String
    private let userImage:String
    private let userDescription:String
    
    init(userId:String,firstName:String,lastName:String,email:String,phone:String,street:String,city:String,country:String,userImage:String,userDescription:String) {
        self.userId = userId
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
        self.street = street
        self.city = city
        self.country = country
        self.userImage = userImage
        self.userDescription = userDescription
    }
    
    internal func getUserId() -> String {
        return userId
    }
    
    internal func getFirstName() -> String {
        return firstName
    }
    
    internal func getLastName() -> String {
        return lastName
    }
    
    internal func getEmail() -> String {
        return email
    }
    
    internal func getPhone() -> String {
        return phone
    }
    
    internal func getStreet() -> String {
        return street
    }
    
    internal func getCity() -> String{
        return city
    }
    
    internal func getCountry() -> String{
        return country
    }
    
    internal func getUserImage() -> String {
        return userImage
    }
    
    internal func getUserDescription() -> String {
        return userDescription
    }
    
}