//
//  Constants.swift
//  Contractual
//
//  Created by Apple on 7/25/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

let CONTRACTUALURL:String = "http://www.pagodalabs.com/taskme/"

let TOAST_INTERVAL : NSTimeInterval = 1000
// Login
let NO_CONNECTION:String = "No Internet Connection"
let INVALID_CREDENTIAL:String = "Invalid Email or Password"
let EMPTY_FIELD:String = "Form Field cannot be Empty"
let INVALID_EMAIL = "Invalid Email Address"
let LOGIN_POST = CONTRACTUALURL + "auth/api/login/"

//Register
let REGISTERURL = CONTRACTUALURL + "job/register_user/"

//Job Image Url
let IMAGE_URL_JOB = CONTRACTUALURL + "uploads/jobs/"
let IMAGE_URL_BANNER = CONTRACTUALURL + "uploads/banners/"
let IMAGE_URL_USERS = CONTRACTUALURL + "uploads/users/"
let IMAGE_URL_PAGE = CONTRACTUALURL + "uploads/page/"

//latest Jobs
let LATEST_JOBS = CONTRACTUALURL + "job/getJobs?&id="

//applied Jobs
let APPLIED_JOBS = CONTRACTUALURL + "job/getAppliedJobs?&id="

//userProfile
let PROFILE = CONTRACTUALURL + "job/getEmployee?&id="

//EMployer profile
let EMPLOYERPROFILE = CONTRACTUALURL + "job/getEmployer?&id="

//apply for job
let APPLYJOB = CONTRACTUALURL + "job/json_apply/"

//EMPLOYER
//posted Jobs
let POSTEDJOBS = CONTRACTUALURL + "job/getPostedJobs?&id="

//job category
let JOBCATEGORY = CONTRACTUALURL + "job/getJobCategory/"

//delete job
let DELETEJOB = CONTRACTUALURL + "job/deleteJob?&id="

//applicants
let APPLICANTS = CONTRACTUALURL + ""

//post job
let POSTJOB = CONTRACTUALURL + "job/postJob/"

//upload multiple image
let UPLOAD_MULTIPLE_IMAGES = CONTRACTUALURL + "job/upload_multiple_image/"


//Swift Spinner Constants
let TAPRETURN = "Tap to Return"
let SOMETHINGWENTWRONG = "Ooops, Something went wrong...."
let NOAPPLIEDJOBS = "You haven't applied to any jobs yet.."
let APPLYFORJOB = "Applying....."
