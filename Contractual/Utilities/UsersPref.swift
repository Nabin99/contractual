//
//  UserDefaults.swift
//  Contractual
//
//  Created by Apple on 7/27/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

class UsersPref : NSObject {
    
    func getId() -> String {
        let prefs = NSUserDefaults.standardUserDefaults()
        let id = prefs.valueForKey("user_id") as! String
        return id
    }
    
    func getUsername() -> String {
        let prefs = NSUserDefaults.standardUserDefaults()
        let username = prefs.valueForKey("username") as! String
        return username
    }
    
    func getEmail() -> String {
        let prefs = NSUserDefaults.standardUserDefaults()
        let email = prefs.valueForKey("email") as! String
        return email
    }
    
    func removePrefs(){
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.removeObjectForKey("user_id")
        prefs.removeObjectForKey("username")
        prefs.removeObjectForKey("email")
    }
}
