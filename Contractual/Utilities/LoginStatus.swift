//
//  LoginStatus.swift
//  Contractual
//
//  Created by Apple on 7/27/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

class LoginStatus: NSObject {
    
    func isLoggedIn() -> String {
        let prefs = NSUserDefaults.standardUserDefaults()
        if prefs.stringForKey("user_id") != nil {
            let group = prefs.stringForKey("group")
            return group!
        }
        return "false"
    }
}