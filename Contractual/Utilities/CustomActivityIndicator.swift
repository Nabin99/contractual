//
//  CustomActivityIndicator.swift
//  Contractual
//
//  Created by Apple on 7/29/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import DTIActivityIndicator

class CustomActivityIndicator : NSObject {
    
    let myActivityIndicatorView: DTIActivityIndicatorView!
    let view:UIView!
    
    init(view:UIView) {
        self.view = view
        myActivityIndicatorView = DTIActivityIndicatorView(frame: CGRect(x:0.0, y:0.0, width:80.0, height:80.0))
        myActivityIndicatorView.center = view.center
        myActivityIndicatorView.indicatorColor = UIColor.redColor()
        //myActivityIndicatorView.indicatorStyle = DTIIndicatorStyle.convInv(.chasingDots)
    }
    
    func start(){
        view.addSubview(myActivityIndicatorView)
        myActivityIndicatorView.startActivity()
    }
    
    func stop(){
        myActivityIndicatorView.stopActivity()
    }
}
