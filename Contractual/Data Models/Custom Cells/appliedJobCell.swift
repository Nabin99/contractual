//
//  appliedJobCell.swift
//  Contractual
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class appliedJobCell: UITableViewCell {
    
    
    @IBOutlet weak var imageName: UIImageView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var jobProvider: UILabel!
    @IBOutlet weak var jobDetails: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func layoutSubviews() {
        imageName.layer.cornerRadius = 5
        imageName.clipsToBounds = true
    }

}
