//
//  Inbox.swift
//  Contractual
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

class Inbox: NSObject {
    
    private let title:String?
    private let date:String?
    private let imageName:String?
    private let details:String?
    
    init(title:String,date:String,imageName:String,details:String) {
        self.title = title
        self.date = date
        self.imageName = imageName
        self.details = details
    }
    
    internal func getTitle() -> String {
        return title!
    }
    
    internal func getDate() -> String {
        return date!
    }
    
    internal func getImageName() -> String{
        return imageName!
    }
    
    internal func getDetails() -> String {
        return details!
    }
}
