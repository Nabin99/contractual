//
//  JobCategory.swift
//  Contractual
//
//  Created by Apple on 8/3/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation


class JobCategory: NSObject {
    
    private let categoryId:String?
    private let title:String?
    private let parentId:String?
    private let status:String?
    
    init(title:String,categoryId:String,parentId:String,status:String) {
        self.title = title
        self.categoryId = categoryId
        self.parentId = parentId
        self.status = status
    }

internal func getTitle() -> String{
    return title!
}

internal func getCategoryId() -> String {
    return categoryId!
}

internal func getParentId() -> String{
    return parentId!
}

internal func getStatus() -> String{
    return status!
}
}