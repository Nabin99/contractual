//
//  JobDetails.swift
//  Contractual
//
//  Created by Apple on 7/22/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

class JobDetails:NSObject {
    
    private let title:String?
    private let imageName:String?
    private let jobProvider:String?
    private let address:String?
    private let startDate:String?
    private let noOfPeople:String?
    private let compensation:String?
    private let location:String?
    
    init(title:String,imageName:String,jobProvider:String,address:String,startDate:String,noOfPeople:String,compensation:String,location:String){
        self.title = title
        self.imageName = imageName
        self.jobProvider = jobProvider
        self.address = address
        self.startDate = startDate
        self.noOfPeople = noOfPeople
        self.compensation = compensation
        self.location = location
    }
    
    internal func getTitle() -> String {
        return title!
    }
    
    internal func getImageName() -> String {
        return imageName!
    }
    
    internal func getJobProvider() -> String {
        return jobProvider!
    }
    
    internal func getAddress() -> String {
        return address!
    }
    
    internal func getStartDate() -> String {
        return startDate!
    }
    
    internal func getNoOfPeople() -> String {
        return noOfPeople!
    }
    
    internal func getCompensation() -> String {
        return compensation!
    }
    
    internal func getLocation() -> String {
        return location!
    }
}