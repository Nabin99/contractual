//
//  User.swift
//  Contractual
//
//  Created by Apple on 7/26/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

class User: NSObject{
    
    private let username:String?
    private let email:String?
    private let id:String?
    private let group:String?
    
    init(username:String,email:String,id:String,group:String) {
        self.username = username
        self.email = email
        self.id = id
        self.group = group
    }
    
    internal func getUsername() -> String{
        return username!
    }
    
    internal func getEmail() -> String{
        return email!
    }
    
    internal func getId() -> String{
        return id!
    }
    
    internal func getGroup() -> String {
        return group!
    }
    
}