//
//  Job.swift
//  Contractual
//
//  Created by Apple on 7/15/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import SwiftyJSON

class Job: NSObject{
    
    private let job_id:String
    private let title:String
    private let user_id:String
    private let imageName:String
    private let jobProvider:String
    private let jobDetails:String
    private let jobType:String
    private let address:String
    private let contact:String
    private let email:String
    private let startDate:String
    private let noOfPeople:String
    private let compensation:String
    private let location:String

    
    

init(job_id:String,title:String,user_id:String,imageName:String,jobProvider:String,jobDetails:String,jobType:String,address:String,contact:String,email:String,startDate:String,noOfPeople:String,compensation:String,location:String){
        self.job_id=job_id
        self.title = title
        self.imageName = imageName
        self.user_id = user_id
        self.jobProvider = jobProvider
        self.jobDetails=jobDetails
        self.jobType = jobType
        self.address = address
        self.contact = contact
        self.email = email
        self.startDate = startDate
        self.noOfPeople = noOfPeople
        self.compensation = compensation
        self.location = location
    }
    
    
    internal func getJobId() -> String{
       return job_id
    }
    
    internal func getTitle() -> String {
       return title
    }
    
    internal func getImageName() -> String {
        return imageName
    }
    
    internal func getJobProvider() -> String {
       return jobProvider
    }
    
    internal func getJobDetails() -> String {
       return jobDetails
    }
    
    internal func getAddress() -> String {
       return address
    }
    
    internal func getContact() -> String{
       return contact
    }
    
    internal func getEmail() -> String{
       return email
    }
    
    internal func getStartDate() -> String {
        return startDate
    }
    
    internal func getNoOfPeople() -> String {
        return noOfPeople
    }
    
    internal func getCompensation() -> String {
        return compensation
    }
    
    internal func getLocation() -> String {
        return location
    }
}
