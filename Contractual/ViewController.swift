//
//  ViewController.swift
//  Contractual
//
//  Created by Apple on 7/13/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeImage.layer.cornerRadius = homeImage.frame.size.width/2
        homeImage.clipsToBounds = true
        signInButton.layer.cornerRadius = 5
        emailTextField.frame.size.height = 100
        passwordTextField.frame.size.height = 100
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }


}

