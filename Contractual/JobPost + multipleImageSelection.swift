//
//  JobPost + multipleImageSelection.swift
//  Contractual
//
//  Created by Apple on 8/17/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import YangMingShan
import Alamofire
import JLToast

extension JobPostController {
    
    func addMoreImages(){
        
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        
        //Customized Theme
        let customColor = UIColor.init(red: 64.0/255.0, green: 0.0, blue: 144.0/255.0, alpha: 1.0)
        let customCameraColor = UIColor.init(red: 86.0/255.0, green: 1.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        pickerViewController.theme.titleLabelTextColor = UIColor.whiteColor()
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.whiteColor()
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.whiteColor()
        pickerViewController.theme.statusBarStyle = .LightContent
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .Alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL.init(string: UIApplicationOpenSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .Alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL.init(string: UIApplicationOpenSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewController(picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        
        self.indicator.start()
        picker.dismissViewControllerAnimated(true) {
            
            
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .HighQualityFormat
            options.resizeMode = .Exact
            options.synchronous = true
            let mutableImages: NSMutableArray! = []
           
            
            for asset: PHAsset in photoAssets
            {
                imageManager.requestImageForAsset(asset, targetSize:PHImageManagerMaximumSize, contentMode: .AspectFill, options: options, resultHandler: { (image, info) in
                    mutableImages.addObject(image!)
                })
            }
        
            
            for images in mutableImages {
                let imageData: NSData = NSData(data: UIImageJPEGRepresentation(images as! UIImage, 0.5)!)
                print(imageData.length/1024)
                let imageString = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                self.imageStringArray.addObject(imageString)
                print(self.imageStringArray.count)
            }
          
            if(self.jobId != 0){
                self.addImagesToServer()
             } else {
                JLToast.makeText("Oops..something Went wrong")
                self.indicator.stop()
            }
          
        }
    }
    
    func addImagesToServer(){
        self.indicator.start()
        self.view.userInteractionEnabled = false
        do {
        let data = try NSJSONSerialization.dataWithJSONObject(imageStringArray, options: NSJSONWritingOptions.PrettyPrinted)
            
            let dataString = NSString(data: data, encoding: NSUTF8StringEncoding)
            let parameters = ["job_image": dataString!,"job_id":self.jobId]
            Alamofire.request(.POST,UPLOAD_MULTIPLE_IMAGES,parameters: parameters)
                .responseJSON{ response in
                    
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            print(value)
                            if value["success"] as! Bool == true {
                                let alertController:UIAlertController = UIAlertController(title: "Upload Successful", message: "You have successfully uploaded images", preferredStyle: .Alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: {action in self.goToHomePage(1)}))
                                self.presentViewController(alertController, animated: true, completion: nil)

                            }
                            else {
                                self.showAlert()
                            }
                        }
                    case .Failure(let error):
                        JLToast.makeText(error.localizedDescription).show()
                        self.showAlert()
                    }
                    self.indicator.stop()
                    self.view.userInteractionEnabled = true
            }

        } catch {
            JLToast.makeText("Couldnt Upload Images").show()
        }
       
    }
    
    func showAlert(){
        let alertController:UIAlertController = UIAlertController(title: "Upload Failed", message: "Something Went Wrong while uploading... Do you want to retry?", preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Retry", style: .Default, handler: {action in self.addImagesToServer()}))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: {action in self.goToHomePage(1)}))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}