//
//  FindJob + searchControl.swift
//  Contractual
//
//  Created by Apple on 8/21/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation

extension FindJobController {
    
  
   
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filteredJobs = jobList.filter { Job in
            return Job.getTitle().lowercaseString.containsString(searchText.lowercaseString)
        }
        
        if(filteredJobs.count == 0){
            searchActive = false
        } else {
            searchActive = true
        }
        self.tableView.reloadData()
    }
    
}